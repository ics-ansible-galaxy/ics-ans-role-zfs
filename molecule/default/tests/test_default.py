import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_zfs_command(host):
    cmd = host.run('/usr/sbin/zfs list')
    assert cmd.rc == 0


def test_zfs_zpool(host):
    cmd = host.run('/usr/sbin/zpool status')
    assert cmd.stdout != 'no pools available'
