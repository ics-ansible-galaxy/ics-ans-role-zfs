ics-ans-role-zfs
===================

Ansible role to setup a zfs filesystem under a zpool.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
# zpool setup
zfs_pool_name: "nas01"
zfs_pool_mode: "mirror"
zfs_pool_devices: "/dev/sdb /dev/sdc"
# ZFS filesystems
zfs_fs:
        - "{{ pool_name }}/labVM"
        - "{{ pool_name }}/labISO"

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ics-ans-role-zfs
```

License
-------

BSD 2-clause
